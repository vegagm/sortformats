# |/usr/bin/env python3


import sys

# Ordered list of image formats, from lower to higher compression
# (assumed classification)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")


def lower_than(format1: str, format2: str) -> bool:

    if fordered.index(format1) < fordered.index(format2):
        return True
    else:
        return False

def find_lower_pos(formats: list, pivot: int) -> int:
    lower : int = pivot
    for pos in range(pivot + 1 , len(formats)):
        if fordered.index(formats[pos]) > fordered.index(formats[lower]):
            lower = pos
    return lower


def sort_formats(formats: list) -> list:
    for pivot_pos in range(len(formats)):
        lower_pos: int = find_lower_pos(formats,pivot_pos)
        if lower_pos != pivot_pos:
            (formats[pivot_pos], formats[lower_pos]) = (formats[lower_pos], formats[pivot_pos])
    return formats


def main():

    formats: list = sys.argv[1:]

    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        print(format, end=" ")
    print()


if __name__ == '__main__':
    main()
